public class SimpleWar{
	public static void main(String args[]){
		//1
		Deck deck = new Deck();
		deck.shuffle();
		//2
		System.out.println("Step 2:");
		System.out.println(deck.toString());
		//3
		int player1 = 0;
		int player2 = 0;
		int round = 1;
		
		//4
		/*System.out.println("Step 4:");
		System.out.println(deck.drawTopCard().toString());
		System.out.println(deck.drawTopCard().toString());*/
		
		//6
		/*System.out.println("Step 6:");
		Card c = deck.drawTopCard();
		System.out.println(c.toString());
		System.out.println(c.calculateScore());*/
		
		//7
		/*System.out.println("Step 7:");
		Card c1 = deck.drawTopCard();
		System.out.println("Card 1:" + c1.toString());
		System.out.println("Score 1:" + c1.calculateScore());
		Card c2 = deck.drawTopCard();
		System.out.println("Card 2:" + c2.toString());
		System.out.println("Score 2:" + c2.calculateScore());*/
		
		//8
		System.out.println("Welcome to Simple War Game!");
		while (deck.length() >=2){
			System.out.println("Round: " + round + "\n" + "-----------------------");
			
			System.out.println("Player 1 point: " + player1 + "\n" + "Player 2 point: " + player2);
			Card c1 = deck.drawTopCard();
			System.out.println("Card 1: " + c1.toString());
			double score1 = c1.calculateScore();
			System.out.println("Score of this card:" + score1);
			
			Card c2 = deck.drawTopCard();
			System.out.println("Card 2: " + c2.toString());
			double score2 = c2.calculateScore();
			System.out.println("Score of this card: " + score2);
			
			if (score1 > score2){
				System.out.println("Player 1 wins!");
				player1 ++;
			}
			else{ 
				System.out.println("Player 2 wins!");
				player2 ++;
			}
			
			System.out.println("Player 1 point: " + player1 + "\n" + "Player 2 point: " + player2 + "\n" + "*****************************************");
			round ++;
		}
		if (player1 > player2)
			System.out.println("Congratulation player 1!");
		else if (player2 > player1)
			System.out.println("Congratulation player 2!");
		else 
			System.out.println("Congratulation 2 players");
	}
}