public class Card{
	//attributes
					  //Ace, ...      J   Q   K
	private int rank; //1, 2, 3, ..., 11, 12, 13
	private String suit; //Hearts, Clubs, Spades, Diamonds
	
	//Constructor
	public Card(int rank, String suit){
		this.rank = rank;
		this.suit = suit;
	}
	
	//getter
	public int getRank(){
		return this.rank;
	}
	public String getSuit(){
		return this.suit;
	}
	
	
	public String toString(){
		String printRank = Integer.toString(this.rank);
		if (this.rank == 1)
			printRank = "ACE";
		if (this.rank == 11)
			printRank = "JACK";
		if (this.rank == 12)
			printRank = "QUEEN";
		if (this.rank == 13)
			printRank = "KING";
		return printRank + " of " + this.suit;
	}
	//method calculate score of a card
	public double calculateScore(){
		double score = 0;
		//adds rank to score
		score += this.rank;
		//adds suit to score
		if (this.suit.equals("HEARTS"))
			score += 0.4;
		if (this.suit.equals("SPADES"))
			score += 0.3;
		if (this.suit.equals("DIAMONDS"))
			score += 0.2;
		if (this.suit.equals("CLUBS"))
			score += 0.1;
		return score;
	}
}